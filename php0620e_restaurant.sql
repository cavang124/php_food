-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2020 at 05:08 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php0620e_restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cate`
--

CREATE TABLE `tbl_cate` (
  `id` int(11) NOT NULL,
  `name_cate` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_cate` datetime NOT NULL DEFAULT current_timestamp(),
  `status_cate` tinyint(4) NOT NULL DEFAULT 1,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_cate`
--

INSERT INTO `tbl_cate` (`id`, `name_cate`, `date_cate`, `status_cate`, `description`) VALUES
(1, 'Bánh ngọt', '2020-10-18 22:47:31', 1, ''),
(2, 'Đồ ăn mặn', '2020-10-18 22:47:31', 1, ''),
(4, 'Đồ ăn chay', '2020-10-18 22:47:31', 1, ''),
(5, 'Đồ uống', '2020-10-18 22:52:25', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_order`
--

CREATE TABLE `tbl_detail_order` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_detail_order`
--

INSERT INTO `tbl_detail_order` (`order_id`, `product_id`, `qty`) VALUES
(1, 20, 1),
(1, 8, 2),
(2, 7, 3),
(3, 10, 1),
(3, 19, 1),
(4, 15, 1),
(1, 20, 2),
(1, 18, 1),
(1, 43, 1),
(24, 18, 1),
(24, 43, 1),
(24, 6, 1),
(24, 10, 1),
(24, 9, 1),
(24, 20, 1),
(24, 6, 1),
(24, 10, 1),
(24, 9, 1),
(24, 20, 1),
(28, 6, 1),
(28, 10, 1),
(28, 9, 1),
(28, 20, 1),
(29, 3, 1),
(29, 8, 1),
(30, 4, 1),
(30, 5, 1),
(31, 19, 1),
(32, 18, 1),
(32, 9, 1),
(32, 16, 1),
(33, 3, 1),
(33, 13, 1),
(33, 16, 1),
(34, 10, 1),
(34, 5, 1),
(35, 3, 1),
(35, 7, 1),
(36, 3, 1),
(36, 43, 1),
(36, 19, 1),
(36, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_img_detail`
--

CREATE TABLE `tbl_img_detail` (
  `product_id` int(11) NOT NULL,
  `img` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_img_detail`
--

INSERT INTO `tbl_img_detail` (`product_id`, `img`) VALUES
(43, 'mucnhoithithap.jpg'),
(43, 'muc-nhoi-thit-hap2.jpg'),
(3, 'Victoria-Sponge2.jpg'),
(3, 'Victoria-Sponge3.jpg'),
(4, 'banh-pho-mat2.jpg'),
(4, 'banh-pho-mat3.jpg'),
(5, 'Swedish_Princess2.jpg'),
(5, 'Swedish_Princess3.jpg'),
(6, 'black-forest2.jpg'),
(6, 'black-forest3.jpg'),
(7, 'pavlova2.jpg'),
(7, 'pavlova3.jpg'),
(18, 'suon-xao-chua-ngot2.jpg'),
(18, 'suon-xao-chua-ngot3.jpg'),
(19, 'ga-hap-mo-hanh3.jpg'),
(19, 'ga-hap-mo-hanh2.jpg'),
(20, 'ca-hoi-nuong-tieu2.jpg'),
(20, 'ca-hoi-nuong-tieu3.jpg'),
(21, 'ca-chem-hap-xi-dau2.jpg'),
(21, 'ca-chem-hap-xi-dau3.jpg'),
(23, 'vit-om-sau2.jpg'),
(23, 'vit-om-sau3.jpg'),
(12, 'gio-lua-chay2.jpg'),
(12, 'gio-lua-chay3.jpg'),
(13, 'cha-que-chay2.jpg'),
(13, 'cha-que-chay3.jpg'),
(16, 'ruoc-nam-chay2.jpg'),
(16, 'ruoc-nam-chay3.jpg'),
(8, 'tra-dao-cam-sa2.png'),
(8, 'tra-dao-cam-sa3.jpg'),
(9, 'chocolate-da-xay2.jpg'),
(9, 'chocolate-da-xay3.jpg'),
(10, 'cookie-cream2.jpg'),
(10, 'cookie-cream3.jpg'),
(11, 'tra-kem-pho-mai2.jpg'),
(11, 'tra-kem-pho-mai3.jpg'),
(61, '1606225768canh-nam-chay2.jpg'),
(61, '1606225768canh-nam-chay3.jpg'),
(15, 'Ca-vien-chay2.jpg'),
(15, 'Ca-vien-chay3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member`
--

CREATE TABLE `tbl_member` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `passw` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_member`
--

INSERT INTO `tbl_member` (`id`, `name`, `phone`, `email`, `passw`, `address`) VALUES
(1, 'Nguyễn Thị Ngọc Bích', '0987888999', 'ngocbichh@gmail.com', '202cb962ac59075b964b07152d234b70', 'Phùng Hưng, Phúc La, Hà Đông'),
(2, 'Lê Quang Bình', '0987888123', 'quangbinh@gmail.com', '202cb962ac59075b964b07152d234b70', 'Mỗ Lao, Hà Đông'),
(3, 'Nguyễn Trung Quân', '0987888332', 'trungquan@gmail.com', '202cb962ac59075b964b07152d234b70', 'Phùng Khoang, Thanh Xuân'),
(4, 'Nguyễn Công Hảo', '0386632922', 'conghao@gmail.com', '202cb962ac59075b964b07152d234b70', 'Phùng Hưng, Phúc La, Hà Đông'),
(10, 'Nguyễn Hoài An', '076565334', 'hoaian@gmail.com', '202cb962ac59075b964b07152d234b70', 'Học viện Công nghệ Bưu chính Viễn thông'),
(17, 'Nguyễn Ngân Anh', '034823343', 'ngananh@gmail.com', '202cb962ac59075b964b07152d234b70', 'Học viện Công nghệ Bưu chính Viễn thông'),
(18, 'Trần Thị A', '0397867874', 'thia@gmail.com', '202cb962ac59075b964b07152d234b70', 'Phùng Hưng, Phúc La, Hà Đông'),
(19, 'php0602', '123', 'php@gmail.com', '202cb962ac59075b964b07152d234b70', ''),
(20, 'Đinh Tuyết Nhi', '0967452121', 'tuyetnhi@gmail.com', '202cb962ac59075b964b07152d234b70', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders`
--

CREATE TABLE `tbl_orders` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `note` varchar(150) NOT NULL,
  `status_order` tinyint(4) NOT NULL DEFAULT 0,
  `total` float NOT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_orders`
--

INSERT INTO `tbl_orders` (`id`, `member_id`, `note`, `status_order`, `total`, `create_at`) VALUES
(1, 1, 'Đồ ăn không thêm ớt', 1, 350000, '2020-11-19 12:16:18'),
(2, 2, 'Giao tận cửa', 0, 0, '2020-11-19 12:16:42'),
(3, 3, '', 1, 0, '2020-11-19 12:17:00'),
(4, 1, '', 2, 0, '2020-11-19 12:17:13'),
(24, 1, 'giao tận nơi', 0, 445000, '2020-11-24 19:27:34'),
(28, 17, 'dfdff', 0, 555000, '2020-11-24 19:35:51'),
(29, 4, 'giảm 50% đá', 1, 310000, '2020-11-24 19:44:07'),
(30, 4, '', 0, 495000, '2020-11-24 19:44:41'),
(31, 4, '', 0, 325000, '2020-11-24 19:47:09'),
(32, 4, '', 0, 395000, '2020-11-24 19:48:32'),
(33, 10, '', 0, 545000, '2020-11-25 09:53:52'),
(34, 1, '', 0, 335000, '2020-11-26 00:24:50'),
(35, 2, '', 0, 555000, '2020-11-26 14:30:03'),
(36, 18, '', 0, 1045000, '2020-11-26 14:39:08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `cate_id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `cate_id`, `name`, `img`, `price`, `quantity`, `slug`, `description`, `create_at`, `status`) VALUES
(3, 1, 'BÁNH VICTORIA SPONGE', 'Victoria-Sponge.jpg', 250000, 5, 'banh-ngot-victoria-sponge', 'Đây là loại bánh được đặt theo tên của nữ hoàng Anh Victoria. Một chiếc Victoria Sponge truyền thống gồm mứt và lượng kem nhiều gấp đôi. Bánh thường được dùng trong tiệc trà chiều của người Anh.', '2020-10-31 15:29:33', 1),
(4, 1, 'BÁNH PHO MÁT', 'banh_pho_mat.jpg', 200000, 10, 'banh-pho-mat-My', 'Bánh ngọt pho mát Mỹ là loại bánh ngọt được làm chủ yếu từ phô mai, tạo vị béo ngậy. Phía trên người ta có thể phủ thêm mứt. Chiếc bánh pho mát kem được làm từ những năm 1800 và trở thành một trong những món bánh quen thuộc của người dân New York.', '2020-10-31 15:29:33', 1),
(5, 1, 'BÁNH SWEDISH PRINCESS', 'Swedish-Princess.jpg', 270000, 8, 'banh-swedish-princess', 'Bánh công chúa của Thụy Điển là món bánh ngọt truyền thống, nhiều người yêu thích. Ban đầu, bánh được làm để phục vụ hoàng gia, được làm từ mứt, trứng, sữa, kem và cốt bánh bông lan, bao phủ phía trên là lớp bánh hạnh nhân (thường có màu xanh). Vete-Katten, Thụy Điển là nơi tuyệt nhất để thưởng thức món bánh mang tên đài các này.', '2020-10-31 15:34:08', 1),
(6, 1, 'BÁNH BLACK FOREST', 'Black-Forest.jpg', 180000, 6, 'banh-black-forest', 'Đây là một kiểu bánh có nguồn gốc từ Đức với tên gốc là Schwarzwälder Kirschtorte. Nó bao gồm nhiều lớp bánh chocolate sponge, phủ váng sữa đánh tơi, maraschino cherry và chocolate bào mỏng.', '2020-10-31 15:36:45', 1),
(7, 1, 'BÁNH PAVLOVA', 'Pavlova-New-Zealand.jpg', 280000, 9, 'banh-pavlova', 'Loại bánh này được đặt theo tên của vũ công ballet hàng đầu của nước Nga, Anna Pavlova. Đây là món bánh được tạo ra để vinh danh cô khi Pavlova đi lưu diễn ở Australia và New Zealand trong những năm 1920. Loại bánh này được làm từ lòng trắng trứng đánh bông với đường, bên ngoài là lớp vỏ cứng nhưng xốp, bên trong là lớp kẹo dẻo. Bánh hay được ăn kèm với kem tươi đánh bông và hoa quả tươi, các loại quả dâu, kiwi...', '2020-10-31 15:36:45', 1),
(8, 5, 'Trà đào cam sả', 'tra-dao-cam-sa.png\r\n', 35000, 10, 'tra-dao-cam-sa', 'Trà đào cam sả là bản hòa tấu tuyệt vời giữa vị ngọt đậm đà của đào, chua nhẹ của cam cùng hương sả thơm nồng. Bên cạnh hương vị độc đáo, dễ uống, trà đào cam sả còn chinh phục đông đảo thực khách bởi các công dụng về sức khỏe. Thưởng thức trà đào cam sả là cách tuyệt vời để thanh lọc cơ thể, nâng cao hệ miễn dịch, giải cảm, giải nhiệt và cải thiện làn da.', '2020-10-31 15:43:18', 1),
(9, 5, 'Socola đá xay', 'socola-da-xay.jpg', 30000, 8, 'socala-da-xay', 'Là một món nước uống nữa được ưa chuộng trở lại trong mùa hè năm nay chính là socola đá xay. Với cái nóng bức mùa hè mà được uống socola đá xay vừa mát lạnh xua tan cái nóng, vừa thơm béo đủ dinh dưỡng cung cấp thêm sức khỏe, sự tỉnh táo thì còn gì tuyệt vời bằng.', '2020-10-31 15:43:18', 1),
(10, 5, 'Cookie Cream ', 'Cookie-Cream.jpg', 40000, 5, 'do-uong-cookie-cream', 'Cookie Cream là một món đồ uống hot trend đang được nhiều người mê mẩn. Đây là một loại đồ uống đá xây với những nguyên liệu như bánh cookie, sữa đặc cùng với kem whipping cream ở bên trên.\r\n\r\n', '2020-10-31 15:46:15', 1),
(11, 5, 'Trà phô mai kem sữa ', 'tra-pho-mai-kem-sua.jpg', 35000, 8, 'tra-pho-mai-kem-sua', 'Nằm trong danh sách những đồ uống ngon Hà Nội chắc hẳn không thể thiếu trà phô mai kem sữa hot rần rần. Mới nghe đến cái tên trà phô mai kem sữa, nhiều người đã thấy thắc mắc, liệu các nguyên liệu này có hòa hợp được với nhau không? Nhưng thực chất nếu được thử một lần ai cũng nghiền luôn món đồ uống vừa béo ngậy, chua ngọt đủ cả mà vẫn có vị thanh của trà.', '2020-10-31 15:46:15', 1),
(12, 4, 'Giò lụa chay', 'Gio-lua-chay.jpg', 120000, 4, 'Gio-lua-chay', 'Giò Lụa Chay là thực phẩm chay phổ dụng nhất thường được ăn, nếu bạn mới ăn chay thì đây là món bạn nên bắt đầu bởi tính dễ ăn và rất dễ chế biến cùng nhiều cách nấu ăn để giò lụa chay thêm hấp dẫn hơn như kho, xào, nấu, hấp,… Giò lụa chay thích hợp làm món ngon trong mâm cỗ hoặc món lẩu hấp dẫn trong những dịp lễ sum họp cùng gia đình, bạn bè. Hiện nay giò lụa chay rất phổ biến trong các quán cơm chay, nhà hàng chay lớn bởi rất phù hợp với mọi nhu cầu ẩm thực của khách hàng.', '2020-10-31 15:55:07', 1),
(13, 4, 'Chả quế chay', 'Cha-que-chay.jpg', 150000, 7, 'Cha-que-chay', 'Chả quế chay là thực phẩm chay rất gần gũi với mâm cơm gia đình, thường dùng trong mâm cỗ chay để đãi khách, bạn bè, tiệc tùng, …. Chả quế chay được chế biến từ 100% tinh bột mỳ, sản phẩm không dùng chất bảo quản nên được bảo quản trong tủ đá, làm sản phẩm luôn tươi ngon khi mang đến tay người tiêu dùng. Hiện nay chả quế chay rất phổ biến trong các quán cơm chay, nhà hàng chay lớn bởi rất phù hợp với mọi nhu cầu ẩm thực của khách hàng.', '2020-10-31 15:55:07', 1),
(14, 4, 'Cá hồng chay', 'Ca-hong-chay.jpg', 200000, 7, 'Ca-hong-chay', 'Cá hồng chay là thực phẩm chay giàu chất dinh dưỡng được chế biến trực tiếp từ 100% đậu nành tươi nguyên chất, đảm bảo sẽ mang tới cho bạn món chay ngon đặc sắc, lành mạnh và giàu chất dinh dưỡng cho cơ thể, sức khỏe. Cá hồng chay thích hợp làm món ngon trong mâm cỗ hoặc món lẩu hấp dẫn trong những dịp lễ sum họp cùng gia đình, bạn bè. Hiện nay cá hồng chay rất phổ biến trong các quán cơm chay, nhà hàng chay lớn bởi rất phù hợp với mọi nhu cầu ẩm thực của khách hàng.', '2020-10-31 15:57:30', 1),
(15, 4, 'Cá viên chay', 'Ca-vien-chay.jpg', 130000, 5, 'ca-vien-chay', 'Cá Viên Chay là thực phẩm chay giàu dinh dưỡng với 100% đậu nành, chế biến được những món ăn chay, dễ làm và đặc biệt tốt cho sức khỏe. Cá viên chay thích hợp làm món ngon trong mâm cỗ hoặc món lẩu hấp dẫn trong những dịp lễ sum họp cùng gia đình, bạn bè. Hiện nay cá viên chay rất phổ biến trong các quán cơm chay, nhà hàng chay lớn bởi rất phù hợp với mọi nhu cầu ẩm thực của khách hàng.', '2020-10-31 15:57:30', 1),
(16, 4, 'Ruốc nấm chay', 'Ruoc-nam-chay-1.jpg', 120000, 5, 'ruoc-nam-chay', 'Ruốc nấm chay là thực phẩm chay giàu chất dinh dưỡng được chế biến trực tiếp từ 100% nấm hương nguyên chất, đảm bảo sẽ mang tới cho bạn món chay ngon đặc sắc, lành mạnh và giàu chất dinh dưỡng cho cơ thể, sức khỏe. Ruốc nấm chay thích hợp làm món ngon trong những dịp lễ sum họp cùng gia đình, bạn bè. Hiện nay ruốc nấm chay rất phổ biến trong các quán cơm chay, nhà hàng chay lớn bởi rất phù hợp với mọi nhu cầu ẩm thực của khách hàng.', '2020-10-31 16:00:53', 1),
(18, 2, 'Sườn xào chua ngọt', 'suon-xao-chua-ngot-3.jpg', 220000, 8, 'suon-xao-chua-ngot', 'Sườn xào chua ngọt là món ăn vô cùng quen thuộc nhưng lại được lòng rất nhiều người thưởng thức bởi vị cay cay, ngọt ngọt của gia vị quyện lẫn từng trong miếng sườn. Hơn nữa thịt sườn mềm thơm... vô cùng kích thích vị giác.', '2020-10-31 16:06:20', 1),
(19, 2, 'Gà hấp mỡ hành', 'Thit-ga-hap-mo-hanh.jpg', 300000, 5, 'ga-hap-mo-hanh', 'Gà gấp mỡ hành là món ăn của người Đài Loan, vừa ngon miệng lại dễ làm. Món ăn này ăn cùng cơm trắng rất ngon. Thịt gà mềm, thơm, ngọt thịt, đậm đà thật khó cưỡng. Ngoài ra nó còn rất giàu dinh dưỡng đấy.', '2020-10-31 16:06:20', 1),
(20, 2, 'Cá hồi nướng tiêu chanh', 'ca-hoi-sot-tieu-sl.jpg', 280000, 8, 'ca-hoi-nuong-tieu-chanh', 'Cá hồi nướng chanh thơm ngon độc đáo để các bạn có thể đổi mới được hương vị cho bữa cơm của gia đình trong những dịp cuối tuần. Món ăn có vị thơm mát của những lát chanh tươi được kết hợp cùng với những miếng cá hồi thơm ngon chắc thịt sẽ tạo ra một món ăn hấp dẫn ngon miệng kích thích được các giác quan của của tất cả mọi người.', '2020-10-31 16:09:02', 1),
(21, 2, 'Cá chẽm hấp xì dầu ', 'ca-chem-chung-tuong.jpg', 240000, 5, 'ca-chem-hap-xi-dau', 'Cá chẽm hấp xì dầu được người Hà Nội yêu thích và cách làm cũng dễ, đơn giản. Vị ngọt đậm đà của cá, mùi thơm của xì dầu và các loại gia vị khiến món ăn ngon này vô cùng hấp dẫn, nhất là trong tiết trời se lạnh này.', '2020-10-31 16:09:02', 1),
(23, 2, 'Vịt om sấu', 'lau-vit-om-sau.jpg', 300000, 8, 'Vit-om-xau', 'Vịt om sấu là 1 trong những món ăn không thế thiếu của con người miền Bắc khi tha phương. Đặc biệt vớ mùi vị chua chua thanh thanh của quả sấu, vị béo ngầy ngậy của miếng thịt vịt khiến bạn không thể nào quên.', '2020-10-31 16:12:34', 1),
(43, 2, 'Mực nhồi thịt hấp', 'muc-nhoi-thit-hap.jpg', 200000, 10, 'muc-nhoi-thit-hap', 'Mực nhồi thịt hấp là 1 món ăn rất ngon.  Mực thơm thơm, dai dai quyện cùng thịt và nấm hương béo ngậy thơm ngon, hấp dẫn khó chối từ, là một món cực kì hấp dẫn đối với những tín đồ hải sản.', '2020-11-23 10:56:32', 1),
(61, 4, 'Canh nấm chay ngũ sắc', '1606227550canh-nam-chay3.jpg', 50000, 12, NULL, 'Canh nấm ngũ sắc thanh dịu với vị ngọt tự nhiên từ các loại nấm và rau củ quả. Nước dùng trong vắt, nhẹ nhàng, cùng màu sắc hài hoà là điểm nhấn của món canh hấp dẫn này. Canh nấm ngũ sắc được dùng trong các bữa tiệc chay. Với vị dịu ngọt, thanh mát, canh nấm ngũ sắc đem đến cảm giác dễ chịu cho người thưởng thức, làm giảm bớt độ ngấy của dầu mỡ và rất tốt cho sức khoẻ.', '2020-11-24 20:49:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating`
--

CREATE TABLE `tbl_rating` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `rating` tinyint(5) NOT NULL DEFAULT 1,
  `create_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_rating`
--

INSERT INTO `tbl_rating` (`id`, `member_id`, `product_id`, `rating`, `create_at`) VALUES
(1, 1, 6, 4, '2020-11-25 09:50:46');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT 0,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `phone`, `email`, `role`, `create_at`, `status`) VALUES
(11, 'binhhn', 'e10adc3949ba59abbe56e057f20f883e', '0967019299', 'lebinh299@gmail.com', 0, '2020-11-17 21:55:11', 1),
(21, 'quangbinh', '202cb962ac59075b964b07152d234b70', '0967019299', 'lebinh019299@gmail.com', 0, '2020-11-18 19:42:40', 1),
(23, 'ngocbich', '900150983cd24fb0d6963f7d28e17f72', '0397867874', 'ngocbich@gmail.com', 1, '2020-11-21 13:17:35', 1),
(25, 'congbao', '123', '076564443', 'congbao@gmail.com', 1, '2020-11-22 13:16:33', 1),
(27, 'ngocbich', '202cb962ac59075b964b07152d234b70', '85456454', 'abc@gmail.com', 1, '2020-11-22 15:59:48', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_cate`
--
ALTER TABLE `tbl_cate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_detail_order`
--
ALTER TABLE `tbl_detail_order`
  ADD KEY `product_id` (`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `tbl_img_detail`
--
ALTER TABLE `tbl_img_detail`
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `tbl_member`
--
ALTER TABLE `tbl_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `fk_tbl_cate_cate_tbl_product` (`cate_id`);

--
-- Indexes for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cate`
--
ALTER TABLE `tbl_cate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_member`
--
ALTER TABLE `tbl_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_detail_order`
--
ALTER TABLE `tbl_detail_order`
  ADD CONSTRAINT `order_id` FOREIGN KEY (`order_id`) REFERENCES `tbl_orders` (`id`),
  ADD CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`);

--
-- Constraints for table `tbl_img_detail`
--
ALTER TABLE `tbl_img_detail`
  ADD CONSTRAINT `tbl_img_detail_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  ADD CONSTRAINT `member_id` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`);

--
-- Constraints for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD CONSTRAINT `fk_tbl_cate_cate_tbl_product` FOREIGN KEY (`cate_id`) REFERENCES `tbl_cate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  ADD CONSTRAINT `tbl_rating_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `tbl_member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_rating_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
